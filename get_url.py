from oauthlib.oauth2 import LegacyApplicationClient
from requests_oauthlib import OAuth2Session
import requests
import json
import getpass

client_id = "b5xcyxk3wk4tmq38ap43x2ze"
client_secret = "ApEy6h73TXyTdUX9EMa8c4wG"
username= "support@natilik.com"

print("This script will extract download URL from software.cisco.com for any item. You will need to know PID and full name of the file you want to download.")

password = getpass.getpass("Please provide support@natilik.com password: ")


oauth = OAuth2Session(client=LegacyApplicationClient(client_id=client_id))
token = oauth.fetch_token(token_url='https://cloudsso.cisco.com/as/token.oauth2',
        username=username, password=str(password), client_id=client_id,
        client_secret=client_secret)

pid = input("Please provide the device PID: ")
image_name = input("Please provide filename you want to download: ")


response_name = requests.get("https://api.cisco.com/software/v3.0/metadata/pid/"+pid+"/image_names/" + image_name, headers={'Authorization': 'Bearer ' + token['access_token']})
json_data_name = json.loads(response_name.text)
metadata_trans_id = json_data_name['metadata_response']['metadata_trans_id']
mdfid = json_data_name['metadata_response']['metadata_pid_list'][0]['metadata_mdfid_list'][0]['mdfid']
image_guid = json_data_name['metadata_response']['metadata_pid_list'][0]['metadata_mdfid_list'][0]['software_response_list'][0]['platform_list'][0]['release_list'][0]['image_details'][0]['image_guid']
response_image = requests.get("https://api.cisco.com/software/v3.0/downloads/pid/"+pid+"/mdf_id/"+mdfid+"/metadata_trans_id/"+metadata_trans_id+"/image_guids/"+image_guid, headers={'Authorization': 'Bearer ' + token['access_token']})
json_data_image = json.loads(response_image.text)

print ("====================")
print("Download URL: "+json_data_image['download_info_list'][0]['download_url']+"&access_token="+token['access_token'])
