# Cisco Software Downloader

Simple script which leverage Cisco Support API to extract download URL for any software on http://software.cisco.com

Before you will run the script you will need to know support@natilik.com password, PID of the device for which you are downloading the software and filename of the software you want to download.

For instance PID will be `C9300-24P` and filename `cat9k_iosxe.16.12.04.SPA.bin`

Run the command `python3 get_url.py` and provide support@natilik.com password, PID and filename. Script will return URL which you than copy paste and you can download it in your browser or wget/curl.
